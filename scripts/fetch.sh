#! /bin/sh

URLBASE=http://www.emulab.net/downloads/linux-dkms
TARBALLDIR="$1"
shift

mkdir -p $TARBALLDIR
for file in $@; do
    outfile="$TARBALLDIR/$file"
    tmpfile="$outfile.tmp"
    [ -f "$outfile" ] && continue
    rm -f "$tmpfile"
    wget -O "$tmpfile" "$URLBASE/$file"

    if [ $? -ne 0 ]; then
	rm -f "$tmpfile"
	break
    fi

    mv -f "$TARBALLDIR/$file.tmp" "$TARBALLDIR/$file"
done
