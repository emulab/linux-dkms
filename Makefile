
PKGTYPE ?= $(shell scripts/osstuff.sh -p)

DRIVERS =
ifeq ($(PKGTYPE),deb)
DRIVERS = e1000e igb i40e iavf ixgbe ixgbevf bnxt-en mptsas mptspi mpt3sas megaraid_sas
endif
ifeq ($(PKGTYPE),rpm)
DRIVERS = e1000e i40e iavf ixgbe ixgbevf bnxt-en mptsas mptspi mpt3sas megaraid_sas
endif

INSTALL_DRIVERS   := $(addsuffix -install,$(DRIVERS))
CLEAN_DRIVERS     := $(addsuffix -clean,$(DRIVERS))
CLEAN_ALL_DRIVERS := $(addsuffix -cleanall,$(DRIVERS))

.PHONY: all install clean

all: $(DRIVERS)

install: $(INSTALL_DRIVERS)

clean: $(CLEAN_DRIVERS)

cleanall: $(CLEAN_ALL_DRIVERS)

$(DRIVERS):
	@$(MAKE) -C drivers/$@ $(PKGTYPE)

$(INSTALL_DRIVERS):
	@$(MAKE) -C drivers/$(subst -install,,$@) $(PKGTYPE)-install

$(CLEAN_DRIVERS):
	@$(MAKE) -C drivers/$(subst -clean,,$@) $(PKGTYPE)-clean

$(CLEAN_ALL_DRIVERS):
	@$(MAKE) -C drivers/$(subst -cleanall,,$@) clean
