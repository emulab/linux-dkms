Emulab `linux-dkms` packaging
-----------------------------

This repository contains deb/rpm DKMS package wrappers for Linux kernel
drivers necessary for some Linux distributions whose kernels contain
drivers too old to support new hardware.  This is a problem often
encountered by Emulab admins, now that both Ubuntu and CentOS run
long-term stable kernels by default -- we often find that during the
5-year lifetime of a given image, the stable/release kernel lacks
support for new NIC and disk/controller hardware.

To build, just type

    make && make install

This will build all driver DKMS packages for either the rpm or deb
packaging formats, depending on the format supported by your system, and
install them.  Installation triggers the dkms build/install subsystem to
build the modules from the package-installed source and rebuild the
initramfs/initrd, if applicable.

If you only want a specific module built and installed, and are on a
Debian-based system, you can run

    make -C drivers/i40e deb deb-install

or on an rpm-based system:

    make -C drivers/i40e rpm rpm-install

To remove all build products for the entire tree, you can run

    make cleanall

To remove the debian build products for a particular module (for
instance, the i40e module), you can run

    make -C drivers/i40e deb-clean

or on an rpm-based system:

    make -C drivers/i40e rpm-clean

Simply running `make clean` in the top-level source directory will
remove the build products for the autodetected system packaging format
(deb or rpm).

To view installed DKMS modules, run

    dkms status

To uninstall a dkms module (but leave the package installed), run

    dkms uninstall -m i40e-dkms -v 2.4.10

See also `man dkms`.
