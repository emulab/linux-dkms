%define module megaraid_sas
%define name %{module}-dkms
%define version 07.717.02.00
%define release 0
%define srcdirname %{module}-%{version}

Source: http://www.emulab.net/downloads/linux-dkms/%{module}-%{version}-src.tar.gz
Summary: DKMS version of the megaraid_sas kernel module
Name: %{name}
Version: %{version}
License: GPLv2
Release: 0
BuildArch: noarch
Requires: dkms gcc kernel-devel

%description
DKMS version of the megaraid_sas kernel module.

%prep
%setup -n %{srcdirname}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/src/%{module}-%{version}/
cp -pR %{_builddir}/%{srcdirname}/* %{buildroot}/usr/src/%{module}-%{version}/
cp -p %{_sourcedir}/dkms.conf %{buildroot}/usr/src/%{module}-%{version}/

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root)
%attr(0755,root,root) /usr/src/%{module}-%{version}/

%post
occurrences=`/usr/sbin/dkms status | grep "%{module}" | grep "%{version}" | wc -l`
if [ $occurrences -eq 0 ]; then
    /usr/sbin/dkms add -m %{module} -v %{version}
fi
/usr/sbin/dkms build -m %{module} -v %{version}
/usr/sbin/dkms install -m %{module} -v %{version}
exit 0

%preun
/usr/sbin/dkms remove -m %{module} -v %{version} --all
exit 0

%changelog
* Mon Jun 21 2021 David M. Johnson <johnsond@flux.utah.edu> 07.717.02.00-0
- Updated DKMS version of the megaraid_sas kernel module.
* Wed Oct 07 2020 David M. Johnson <johnsond@flux.utah.edu> 07.715.02.00-0
- Updated DKMS version of the megaraid_sas kernel module.
* Tue Dec 03 2019 David M. Johnson <johnsond@flux.utah.edu> 07.711.04.00-0
- Updated DKMS version of the megaraid_sas kernel module.
* Wed Mar 06 2019 David M. Johnson <johnsond@flux.utah.edu> 07.705.04.00-0
- Initial release of DKMS version of the megaraid_sas kernel module.
