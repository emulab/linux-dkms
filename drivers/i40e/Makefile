
NAME        = i40e
PKGNAME     = $(NAME)-dkms
VERSION    ?= 2.10.19.30
PKGVERSION ?= $(VERSION)-0
BUILDDIR    = build
SOURCEDIR   = $(BUILDDIR)/$(NAME)-$(VERSION)
FILENAME    = $(NAME)-$(VERSION).tar.gz
TARBALLDIR  = ../../tarballs
TARBALL     = $(TARBALLDIR)/$(FILENAME)
FETCH       = ../../scripts/fetch.sh
DEBPKGDIR   = ..
DEBPKGNAME  = $(PKGNAME)_$(PKGVERSION)_all.deb
DEBPKGPATH  = $(DEBPKGDIR)/$(DEBPKGNAME)
RPMPKGPATH  = $(BUILDDIR)/RPMS/noarch/$(PKGNAME)-$(PKGVERSION).noarch.rpm

all:

clean: deb-clean rpm-clean

$(TARBALL):
	$(FETCH) $(TARBALLDIR) $(FILENAME)

$(SOURCEDIR):	$(TARBALL)
	if [ -d $(SOURCEDIR) ]; then rm -rf $(SOURCEDIR); fi
	mkdir -p $(BUILDDIR)
	tar -xzf $(TARBALL) -C $(BUILDDIR)
	touch $(SOURCEDIR)

$(DEBPKGPATH): | $(SOURCEDIR)
	dpkg-buildpackage -us -uc

deb:	$(DEBPKGPATH)

deb-install:	deb
	dpkg -i $(DEBPKGPATH)

deb-clean:
	rm -rf $(SOURCEDIR) $(DEBPKGPATH) $(DEBPKGDIR)/$(PKGNAME)_*

$(BUILDDIR)/SOURCES/$(FILENAME): $(TARBALL)
	mkdir -p $(BUILDDIR)/SOURCES
	cp -p $(TARBALL) $(BUILDDIR)/SOURCES/$(FILENAME)
	cp -p dkms.conf $(BUILDDIR)/SOURCES

$(RPMPKGPATH): rpm/$(PKGNAME).spec $(BUILDDIR)/SOURCES/$(FILENAME)
	mkdir -p $(BUILDDIR)/{BUILD,RPMS/noarch}
	rpmbuild --define "_topdir "$(shell readlink -f $(BUILDDIR)) -ba rpm/$(PKGNAME).spec

rpm:	$(RPMPKGPATH)

rpm-clean:
	rm -rf $(RPMPKGPATH) $(BUILDDIR)/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

rpm-install:	rpm
	rpm -iv $(RPMPKGPATH)

.PHONY: all clean deb deb-install deb-clean
