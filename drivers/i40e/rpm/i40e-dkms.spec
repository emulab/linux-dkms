%define module i40e
%define name %{module}-dkms
%define version 2.10.19.30
%define release 0
%define srcdirname %{module}-%{version}

Source: http://www.emulab.net/downloads/linux-dkms/%{module}-%{version}.tar.gz
Summary: DKMS version of the i40e kernel module
Name: %{name}
Version: %{version}
License: GPLv2
Release: 0
BuildArch: noarch
Requires: dkms gcc kernel-devel

%description
DKMS version of the i40e kernel module.

%prep
%setup -n %{srcdirname}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/src/%{module}-%{version}/
cp -pR %{_builddir}/%{srcdirname}/* %{buildroot}/usr/src/%{module}-%{version}/
echo 'obj-y += src/' > %{buildroot}/usr/src/%{module}-%{version}/Makefile
cp -p %{_sourcedir}/dkms.conf %{buildroot}/usr/src/%{module}-%{version}/

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root)
%attr(0755,root,root) /usr/src/%{module}-%{version}/

%post
occurrences=`/usr/sbin/dkms status | grep "%{module}" | grep "%{version}" | wc -l`
if [ $occurrences -eq 0 ]; then
    /usr/sbin/dkms add -m %{module} -v %{version}
fi
/usr/sbin/dkms build -m %{module} -v %{version}
/usr/sbin/dkms install -m %{module} -v %{version}
exit 0

%preun
/usr/sbin/dkms remove -m %{module} -v %{version} --all
exit 0

%changelog
* Tue Dec 03 2019 David M. Johnson <johnsond@flux.utah.edu> 2.10.19.30-0
- Update i40e DKMS kernel module to version 2.10.19.30.

* Mon Feb 04 2019 David M. Johnson <johnsond@flux.utah.edu> 2.7.29-0
- Update i40e DKMS kernel module to version 2.7.29.

* Thu Sep 06 2018 David M. Johnson <johnsond@flux.utah.edu> 2.4.10-0
- Initial release of DKMS version of the i40e kernel module.
